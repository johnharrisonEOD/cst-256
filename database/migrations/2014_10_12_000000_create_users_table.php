<?php
/* CST-256 Database Application Programming III
 * Milestone 3
 * CreateUserTable, Version 1
 * Group CLC Project
 * 09/29/2019
 * This is used to create the users table required for the project
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('usert')->default('Individual');
            $table->string('status')->default('Active');
            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();
            $table->string('companyName')->nullable();
            $table->string('position')->nullable();
            $table->string('jobDuties')->nullable();
            $table->date('completionDate')->nullable();
            $table->string('universityName')->nullable();
            $table->string('major')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
}
