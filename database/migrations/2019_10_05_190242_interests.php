<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Interests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('interest');
            $table->timestamps();
        });
        
            DB::table('interests')->insert(array('interest' => 'Aerospace'));
            DB::table('interests')->insert(array('interest' => 'Agriculture'));
            DB::table('interests')->insert(array('interest' => 'Art'));
            DB::table('interests')->insert(array('interest' => 'Construction'));
            DB::table('interests')->insert(array('interest' => 'Education'));
            DB::table('interests')->insert(array('interest' => 'Energy'));
            DB::table('interests')->insert(array('interest' => 'Entertainment'));
            DB::table('interests')->insert(array('interest' => 'Food'));
            DB::table('interests')->insert(array('interest' => 'Hospitality'));
            DB::table('interests')->insert(array('interest' => 'Manufacturing'));
            DB::table('interests')->insert(array('interest' => 'Medicine'));
            DB::table('interests')->insert(array('interest' => 'Mining'));
            DB::table('interests')->insert(array('interest' => 'Music'));
            DB::table('interests')->insert(array('interest' => 'News'));
            DB::table('interests')->insert(array('interest' => 'Pharmaceutical'));
            DB::table('interests')->insert(array('interest' => 'Technology'));
            DB::table('interests')->insert(array('interest' => 'Telecommunication'));
            DB::table('interests')->insert(array('interest' => 'Transport'));
    }

    
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('interests');
        Schema::enableForeignKeyConstraints();
        //
    }
}
