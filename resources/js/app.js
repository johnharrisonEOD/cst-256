/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$(document).ready(function()
{
    $('.butts').click(function (event)
    {
        let inter = event.currentTarget.innerHTML;
        event.currentTarget.className += ' active';

        $.get('home',{interests:inter},function ()
        {
            console.log(inter);
        });
    });
});

$(document).ready(function()
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.join').click(function (event)
    {
        event.currentTarget.innerHTML = 'Joined';
        event.currentTarget.className += ' active';
        let name = event.currentTarget.value;
        $.post('findGroups',{group:name},function(group)
        {
            console.log(group);
        });
        location.reload();
    });
});

$(document).ready(function()
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.joined').click(function (event)
    {
        event.currentTarget.innerHTML = 'Join';
        event.currentTarget.className = 'btn-lg btn-outline-primary join';
        let name = event.currentTarget.value;
        $.post('leaveGroups',{group:name},function(group)
        {
            console.log(group);
        });
        location.reload();
    });

    $('.joined').hover(function (event)
    {
        event.currentTarget.innerHTML = 'Leave?';
        event.currentTarget.style.backgroundColor = 'rgb(189,33,48)';
        event.currentTarget.style.borderColor = 'rgb(189,33,48)';
    },function (event)
    {
        event.currentTarget.innerHTML = 'Joined';
        event.currentTarget.style.backgroundColor = 'rgb(0,98,204)';
        event.currentTarget.style.borderColor = 'rgb(0,98,204)';
    });

});
