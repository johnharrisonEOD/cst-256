<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * find_job, Version 1
 * Group CLC Project
 * 10/27/2019
 * This will display a form where a list of jobs will be displayed.
 */
?>

@extends('layouts.app')

@section('title') Find A Job @endsection

@section('content')
    <div class="container" >
        <div class="bg-light" id="findJobContainer">
            <div id="title">
                <h2>Find Your Dream Job</h2>
            </div>
            <hr>
            @if($jobs->count() == 0)
                <h4>No jobs have been posted yet.</h4>
                <p>Check back later!</p>
            
            
            @else
            <form method="get" action="{{route('findJob')}}">
             @csrf
            <div class="row">
                 <div class="col">
                 <div class="form-group">
            	<label for="jobtablefilter">{{__('Job Display: ')}}</label>
                    <select class="form-control" id="JOBTABLEFILTER" name="jobtablefilter">
                    	<option selected="selected" value=""></option>
                    	<option value="All">All Jobs</option>
                    	<option value="Recommended">Recommended Jobs</option>
                    </select> 
                    </div>
                    </div>
                 <div class="col">   
                 	<br>
                    <!-- <button class="btn btn-primary btn-block" type="submit" style="width: 25%">Submit</button> -->
                 </div>
                 <div class="col"></div>
            
            </div>
            
            
            <div class="row">
                 <div class="col">
            	<div class="form-group">
                    <label for="jobsearch">{{__('Job Search: ')}}</label>
                    <input class="form-control" id="JOBSEARCH" type="text" name="jobsearch">
                    
                    </div>
                    </div>
                 <div class="col">   
                 	<br>
                    <!-- <button class="btn btn-primary btn-block" type="submit" style="width: 25%">Submit</button> -->
                 </div>
                 <div class="col"></div>
            </div>  
            
            
            <div class="col">   
                 <br>
                 <button class="btn btn-primary btn-block" type="submit" style="width: 15%">Get Jobs</button>
            </div><hr>
            </form>
            
            
            @foreach($jobs as $j)
                <div id="job">
                    <h4><a href="{{route('viewJob',['id'=>$j->id])}}">{{$companies->find($j->company_id)->name}} - {{$j->name}}</a></h4>
                    <p class="small">{{$j->city}}, {{$j->state}}</p>
                </div>
                <hr>
            @endforeach
            @endif
        </div>
    </div>
@endsection
