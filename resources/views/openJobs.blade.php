<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * openJobs, Version 1
 * Group CLC Project
 * 10/27/2019
 * This will display a list of open jobs for the admin and business users.
 */
?>

@extends('layouts.app')

@section('title') Open Jobs @endsection

@section('content')
    <div class="container" >
            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        <div class="bg-light" id="findJobContainer">
            <div id="title">
                <h2>Open Jobs</h2>
            </div>
            <hr>
            @if($jobs->count() == 0)
                @if(Auth::user()->usert!='Administrator')
                    <h4>No open jobs</h4>
                    <a class="small" href="{{route('createJob')}}">Create new job?</a>
                @else
                    <h4>No open jobs</h4>
                @endif
            @endif
            @foreach($jobs as $j)
                <div id="job">
                    <h4><a href="{{route('editJob',['jobID' => $j->id])}}"> @if(Auth::user()->usert=='Administrator'){{$company->find($j->company_id)->name}} - {{$j->name}} @else {{$j->name}} @endif </a></h4>
                    <p class="small">{{$j->city}}, {{$j->state}}</p>
                    <form action="{{ route('company.destroy', $j->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Close</button>
                    </form>
                </div>
                <hr>
            @endforeach
        </div>
    </div>
@endsection
