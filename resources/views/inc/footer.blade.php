<div class="bg-dark footer-dark shadow">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3 item">
                    <h3>CST-256</h3>
                    <ul>
                        <li><a href="#">Web design</a></li>
                        <li><a href="#">Development</a></li>
                        <li><a href="#">Hosting</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3 item">
                    <h3>Team</h3>
                    <ul>
                        <li><a href="#">Gary Davis</a></li>
                        <li><a href="#">Tyson Cruz</a></li>
                        <li><a href="#">John Harrison</a></li>
                        <li><a href="#">Ramon Leon</a></li>
                        <li></li>
                    </ul>
                </div>
                <div class="col-md-6 item text">
                    <h3>Our Mission</h3>
                    <p>Provide the best professional networking app possible.</p>
                </div>
            </div>
            <p class="copyright">CST-256 - Grand Canyon University © 2019</p>
        </div>
    </footer>
</div>

