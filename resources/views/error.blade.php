<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * Error, Version 1
 * Group CLC Project
 * 10/26/2019
 * This error page will display if there is an error.
 */
?>

@extends('layouts.app')

@section('title') ProNet Error @endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <h3 class="card-header">ProNet Error</h3>
                    <div class="card-body">
                        <div class="row">
                            <div class="col"></div></a></div>
                            <div class="form-group">
                                 <label for="error">An Error has occured, please contact the site support.</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
