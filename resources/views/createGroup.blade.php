<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * findGroups, Version 1
 * Group CLC Project
 * 10/27/2019
 * This will display a form where the user can create a group based on multiple interests.
 */
?>

@extends('layouts.app')

@section('title') Create Group @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <h4 class="card-header">Create Group</h4>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                            @if ($errors->any())
      						<div class="alert alert-danger">
        						<ul>
            					@foreach ($errors->all() as $error)
              					<li>{{ $error }}</li>
            					@endforeach
        						</ul>
      							</div><br />
    							@endif
    							
    							<form method="post" action="{{route('createNewGroup')}}">
                    				@csrf   
                                  	<div class="form-group">
                                  		 <label for="groupname">Group Name:</label>
                                	 	 <input class="form-control" id="NAME" type="text" name="name">
                                	 	 <br>
                                		 <label for="postBody">Group Description:</label>
                                   	   	<textarea class="form-control" id="DESCRIPTION" name="description" rows="3"></textarea>
                                   	   	<br>
                                   	   <!-- <label for="groupinterest">Group Interest (Key Word):</label>
                                	 	<input class="form-control" id="INTEREST" type="text" name="interest"> -->
                                	 	<div class="form-group">
                                        <label for="industry">{{__('Industry: ')}}</label>
                                        <select class="form-control" id="INTEREST" name="interest">
                                            <option selected="selected" value=""></option>
                                            <option value="Aerospace">Aerospace</option>
                                            <option value="Agriculture">Agriculture</option>
                                            <option value="Art">Art</option>
                                            <option value="Construction">Construction</option>
                                            <option value="Education">Education</option>
                                            <option value="Energy">Energy</option>
                                            <option value="Entertainment">Entertainment</option>
                                            <option value="Food">Food</option>
                                            <option value="Hospitality">Hospitality</option>
                                            <option value="Manufacturing">Manufacturing</option>
                                            <option value="Medicine">Medicine</option>
                                            <option value="Mining">Mining</option>
                                            <option value="Music">Music</option>
                                            <option value="News">News</option>
                                            <option value="Pharmaceutical">Pharmaceutical</option>
                                            <option value="Technology">Technology</option>
                                            <option value="Telecommunication">Telecommunication</option>
                                            <option value="Transport">Transport</option>
                                        </select>
                                	 	
                                	 	
                                  	</div>
                                  	<button type="submit" class="btn btn-primary">Create</button>
                                </form>
    							
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>



<br>

<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
@endsection