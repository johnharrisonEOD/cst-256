<?php
/* CST-256 Database Application Programming III
 * Milestone 3
 * editPortfolio, Version 1
 * Group CLC Project
 * 09/22/2019
 * This will display a form where the user portfolio data can be edited and updated
 */
?>

@extends('layouts.app')

@section('title') Portfolio @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <h4 class="card-header">Portfolio</h4>
                    <div class="card-body">
                        <div class="row">
                            <!--  <div class="col"><a href="#"><div id="background"></div></a></div>-->
                            <div class="col">
                            @if ($errors->any())
      						<div class="alert alert-danger">
        						<ul>
            					@foreach ($errors->all() as $error)
              					<li>{{ $error }}</li>
            					@endforeach
        						</ul>
      							</div><br />
    							@endif
                                <form method="post" action="{{route('updatePortfolio', $user->id)}}">
                                	{{csrf_field()}}
                                	@csrf
                                	
                                	
                                    <div class="form-group">
                                        <label for="firstName">{{__('First Name: ')}}</label>
                                        <input class="form-control" id="firstName" type="text" name="firstName" value="{{$user->firstName}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastName">{{__('Last Name: ')}}</label>
                                        <input class="form-control" id="lastName" type="text" name="lastName" value="{{$user->lastName}}">
                                    </div>
                                    <p><strong>Employment History:</strong></p>
                                    <div class="form-group">
                                        <label for="startDate">{{__('Start Date: ')}}</label>
                                        <input class="form-control" id="startDate" type="date" name="startDate" value="{{$user->startDate}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="endDate">{{__('End Date: ')}}</label>
                                        <input class="form-control" id="endDate" type="date" name="endDate" value="{{$user->endDate}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="companyName">{{__('Company Name: ')}}</label>
                                        <input class="form-control" id="companyName" type="text" name="companyName" value="{{$user->companyName}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="position">{{__('Position: ')}}</label>
                                        <input class="form-control" id="position" type="text" name="position" value="{{$user->position}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="jobDuties">{{__('Duties: ')}}</label>
                                        <input class="form-control" id="jobDuties" type="text" name="jobDuties" value="{{$user->jobDuties}}">
                                    </div>
                                    <p><strong>Education:</strong></p>
                                    <div class="form-group">
                                        <label for="completionDate">{{__('Completion Date: ')}}</label>
                                        <input class="form-control" id="completionDate" type="date" name="completionDate" value="{{$user->completionDate}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="universityName">{{__('College/University: ')}}</label>
                                        <input class="form-control" id="universityName" type="text" name="universityName" value="{{$user->universityName}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="major">{{__('Major: ')}}</label>
                                        <input class="form-control" id="major" type="text" name="major" value="{{$user->major}}">
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<br>

<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

@endsection
