<?php
/* CST-256 Database Application Programming III
 * Milestone 4
 * editGroup, Version 1
 * Group CLC Project
 * 10/06/2019
 * This will display a form where the group data can be edited and updated
 */
?>

@extends('layouts.app')

@section('title') Admin Menu @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <h4 class="card-header">Edit Group</h4>
                    <div class="card-body">
                        <div class="row">
                            <div class="col"></div></a></div>
                            <div class="col">
                            @if ($errors->any())
      						<div class="alert alert-danger">
        						<ul>
            					@foreach ($errors->all() as $error)
              					<li>{{ $error }}</li>
            					@endforeach
        						</ul>
      							</div><br />
    						@endif
                                <form method="post" action="{{route('updateGroup', $group->id)}}">
                                	@csrf

                                    <div class="form-group">
                                        <label for="name">{{__('Group Name: ')}}</label>
                                        <input class="form-control" id="NAME" type="text" name="name" value="{{$group->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">{{__('Group Description: ')}}</label>
                                        <input class="form-control" id="DESCRIPTION" type="text" name="description" value="{{$group->description}}">
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>


@endsection
