<?php
/* CST-256 Database Application Programming III
 * Milestone 3
 * editUser, Version 1
 * Group CLC Project
 * 09/22/2019
 * This will display a form where the user data can be edited and updated
 */
?>

@extends('layouts.app')

@section('title') Admin Menu @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <h4 class="card-header">Profile</h4>
                    <div class="card-body">
                        <div class="row">
                            <div class="col"><a href="#"><div id="background"></div></a></div>
                            <div class="col">
                            @if ($errors->any())
      						<div class="alert alert-danger">
        						<ul>
            					@foreach ($errors->all() as $error)
              					<li>{{ $error }}</li>
            					@endforeach
        						</ul>
      							</div><br />
    							@endif
                                <form method="post" action="{{route('admin.update', $user->id)}}">
                                	@csrf
                                	@method('PATCH')

                                    <div class="form-group">
                                        <label for="firstName">{{__('Name: ')}}</label>
                                        <input class="form-control" id="NAME" type="text" name="name" value="{{$user->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{{__('Email: ')}}</label>
                                        <input class="form-control" id="EMAIL" type="text" name="email" value="{{$user->email}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">{{__('Password: ')}}</label>
                                        <input class="form-control" id="PASSWORD" type="password" name="password" value="{{$user->password}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="city">{{__('City: ')}}</label>
                                        <input class="form-control" id="CITY" type="text" name="city" value="{{$user->city}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="state">{{__('State: ')}}</label>
                                        <select class="form-control" id="STATE" name="state" >
                                        	<option selected="selected" value="{{$user->state}}">{{$user->state}}</option>
                                            <option value=""></option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                    </div>
                                    @if($user->usert == 'Individual')
                                        <div class="form-group">
                                            <label for="phone">{{__('Phone: ')}}</label>
                                            <input class="form-control" id="PHONE" type="text" name="phone" disabled value="{{$user->phone}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="gender">{{__('Gender: ')}}</label>
                                            <select class="form-control" id="MF" disabled name="gender">
                                                <option selected="selected" value="{{$user->gender}}">{{$user->gender}}</option>
                                                <option value=""></option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    @elseif($user->usert == 'Business')
                                        <div class="form-group">
                                            <label for="type">{{__('Type of Company: ')}}</label>
                                            <textarea rows="8" cols="80" class="form-control" id="TYPE" type="text" name="type"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="description">{{__('Description of Company: ')}}</label>
                                            <textarea rows="8" cols="80" class="form-control" id="DESCRIPTION" type="text" name="description"></textarea>
                                        </div>
                                    @endif
	                                <div class="form-group">
                                        <label for="usert">{{__('Type Of User: ')}}</label>
                                        <select class="form-control" id="USERT" name="usert" >
                                        	<option selected="selected" value="{{$user->usert}}">{{$user->usert}}</option>
                                            <option value=""></option>
                                            <option value="Individual">Individual</option>
                                            <option value="Business">Business</option>
                                            <option value="Administrator">Administrator</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="status">{{__('Account Status: ')}}</label>
                                        <select class="form-control" id="STATUS" name="status" >
                                        	<option selected="selected" value="{{$user->status}}">{{$user->status}}</option>
                                            <option value=""></option>
                                            <option value="Active">Active</option>
                                            <option value="Suspended">Suspended</option>
                                        </select>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                        </div>
                                        <div class="col">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>


@endsection
