<?php 
/* CST-256 Database Application Programming III
 * Milestone 3
 * Admin index, Version 1
 * Group CLC Project
 * 09/29/2019
 * This will display the admin view with a table of users to create, read, update and delete. Also updates the user portfolio 
 */
?>

@extends('layouts.app')

@section('title') Admin Menu @endsection

@section('content')
<a class="btn btn-primary btn-lg" href="{{ route('create') }}" role="button">Create User</a>
<p>
<div class="container">
    <div class="row justify-content-center">
		<div class="row">
			<div class="card">
    			<h1 class="card-header">Users</h1>    
  				<table class="table table-striped">
    				<thead>
        				<tr>
          				<td>ID</td>
          				<td>Name</td>
          				<td>Email</td>
          				<td>Type</td>
          				<td>Status</td>
          				<td colspan = 2>Actions</td>
        				</tr>
    				</thead>
    				<tbody>
        				@foreach($users as $User)
        				<tr>
            				<td>{{$User->id}}</td>
            				<td>{{$User->name}}</td>
            				<td>{{$User->email}}</td>
            				<td>{{$User->usert}}</td>
            				<td>{{$User->status}}</td>
            				<td>
                			<!-- Split button -->
							<div class="btn-group">
  								<button type="button" class="btn btn-primary">Edit</button>
  								<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    								<span class="caret"></span>
    								<span class="sr-only">Toggle Dropdown</span>
  								</button>
  								<ul class="dropdown-menu">
    								<li><a href="{{ route('admin.edit',$User->id)}}">User Profile</a></li>
    								<li><a href="{{ route('editUserPortfolio',$User->id)}}">User ePortfolio</a></li>
  								</ul>
							</div>
                			<!-- <a href="{{ route('admin.edit',$User->id)}}" class="btn btn-primary">Edit</a> -->
            				</td>
            				<td>
                			<form action="{{ route('admin.destroy', $User->id)}}" method="post">
                  				@csrf
                  				@method('DELETE')
                  			<button class="btn btn-danger" type="submit">Delete</button>
                			</form>
            				</td>

        				</tr>
        				@endforeach
    				</tbody>
  				</table>
			</div>
		</div>
	</div>
</div>
<br>


<div class="container">
    <div class="row justify-content-center">
		<div class="row">
			<div class="card">
    			<h1 class="card-header">Groups</h1>    
  				<table class="table table-striped">
    				<thead>
        				<tr>
          				<td>ID</td>
          				<td>Name</td>
          				<td>Details</td>
          				<td colspan = 2>Actions</td>
        				</tr>
    				</thead>
    				<tbody>
        				@foreach($groups as $Group)
        				<tr>
            				<td>{{$Group->id}}</td>
            				<td>{{$Group->name}}</td>
            				<td>{{$Group->description}}</td>
            				<td>
                			<a href="{{ route('editGroup',$Group->id)}}" class="btn btn-primary">Edit</a>
                			@csrf
            				</td>
            				<td>
                			<form action="{{ route('destroyGroup', $Group->id)}}" method="post">
                  				@csrf
                  				@method('DELETE')
                  			<button class="btn btn-danger" type="submit">Delete</button>
                			</form>
            				</td>

        				</tr>
        				@endforeach
    				</tbody>
  				</table>
			</div>
		</div>
	</div>
</div>
<br>


<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
@endsection