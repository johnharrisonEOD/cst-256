@extends('layouts.app')

@section('title') Register Company @endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <h3 class="card-header">Register Company</h3>
                    <div class="card-body">
                        <div class="row">
                            <div class="col"><a href="#"><div id="background"></div></a></div>
                            <div class="col">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="{{route('registerCompany')}}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">{{__('Company Name: ')}}</label>
                                        <input class="form-control" id="NAME" type="text" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{{__('Email: ')}}</label>
                                        <input class="form-control" name="email" type="email" id="EMAIL" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">{{__('Type: ')}}</label>
                                        <input class="form-control" id="TYPE" name="type" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">{{__('Description: ')}}</label>
                                        <textarea class="form-control" cols="80" rows="8" name="description" id="DESCRIPTION"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="city">{{__('City: ')}}</label>
                                        <input class="form-control" id="CITY" type="text" name="city" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="state">{{__('State: ')}}</label>
                                        <select class="form-control" id="STATE" name="state" required>
                                            <option value=""></option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <div class="row">
                                        <div class="col">
                                            <button class="btn btn-primary btn-block" type="submit" >Register</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
