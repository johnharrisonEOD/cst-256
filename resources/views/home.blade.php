<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * home, Version 1
 * Group CLC Project
 * 10/27/2019
 * This will display the project home page
 */
?>

@extends('layouts.app')

@section('title') Home @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
            @endif
            <div class="card">
                <div class="card-header">Welcome {{Auth::user()->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>New features added:</p>
                    <p>- Groups</p>
                    <p>- Interests</p>
                    <p>Select some interests below and we will recommend some groups you can join with people that share the same interests!</p>
                </div>
            </div>
                <br>
            <div class="bg-light border rounded">
                <div class="container text-center">
                    <br>
                    <h3> What are you interested in?</h3>
                    <br>
                    <div class="text-center" id="buttons">
                    @php($counter = 0)
                       @for($i = 0; $i < count($interests); $i++)
                           @if($counter < 5)
                                @php($counter+=1)
                            @else
                                @php($counter = 0)

                            @endif

                            <button class="btn btn-outline-{{$colors[$counter]}} butts">{{$interests[$i]->interest}}</button>

                        @endfor
                    
                        <!-- <button class="btn btn-outline-warning butts">Technology</button>
                        <button class="btn btn-outline-primary butts">Medicine</button>
                        <button class="btn btn-outline-danger butts">Music</button>
                        <button class="btn btn-outline-success butts">Art</button> -->
                    </div>
                    <br>
                    <a class="btn btn-outline-primary btn-block" href="{{route('findGroups')}}">Find Groups</a>
                    <br>
                    <div id="interests"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

