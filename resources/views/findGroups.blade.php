<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * findGroups, Version 1
 * Group CLC Project
 * 10/27/2019
 * This will display a form where a list of recommended groups will be displayed.
 */
?>

@extends('layouts.app')

@section('title') Find Groups @endsection

@section('content')
    <div class="container" >
        <div class="bg-light" id="findJobContainer">
            <div id="title" class="text-center text-dark">
                <h3>Connect with like minded people in a group based on your interests</h3>
            </div>
            <hr>
            @if($groups->count() == 0)
                <h4>No groups have been created yet.</h4>
                <p>Now seems like a great time to create one!</p>
            @endif
            @foreach($groups as $g)
                <div id="group" class="row">
                    <div class="col-lg">
                        <h4 ><a href="{{route('groupPage',['id'=>$g->id])}}" id="group-name">{{$g->name}}</a></h4>
                        <p>{{$g->description}}</p>
                        <p class="small">Interests in common: @if($g->inCommon == null) {{0}} @else {{$g->inCommon}} @endif </p>
                        @if(!in_array($g->name,$names))
                            <button class="btn-lg btn-outline-primary join" value="{{$g->name}}" >Join</button>
                        @else
                            <button class="btn-lg btn-primary joined" value="{{$g->name}}">Joined</button>
                        @endif
                    </div>
                </div>
                <hr>
            @endforeach
            <div class="text-right"><a class="btn btn-primary" href="{{ route('createGroup') }}">+ Create a Group</a></div>
        </div>
    </div>
    <div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
@endsection
