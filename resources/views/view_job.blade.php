<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * view_job, Version 1
 * Group CLC Project
 * 10/26/2019
 * This will display a form that will be populated with job specific information (user selected)
 */
?>

@extends('layouts.app')

@section('title') Post A Job @endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <h3 class="card-header">Job View</h3>
                    <div class="card-body">
                        <div class="row">
                            <div class="col"> <a href="#"><div id="background"></div></a></div>
                            <div class="col">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div><br />
                                @endif
                                <form method="post" action="{{route('userApply',$job->id)}}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="jobTitle">{{__('Job Title: ')}}</label>
                                        <input class="form-control" id="TITLE" type="text" name="title" disabled value="{{$job->name}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="responsibilities">{{__('Responsibilities: ')}}</label>
                                        <textarea class="form-control" rows="8" cols="80" name="responsibilities" disabled id="RESPONSIBILITIES" >{{$job->responsibilities}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="requirements">{{__('Requirements: ')}}</label>
                                        <textarea class="form-control" rows="8" cols="80" name="requirements" disabled id="REQUIREMENTS">{{$job->requirements}}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="type">{{__('Type: ')}}</label>
                                        <select class="form-control" id="TYPE" disabled name="type">
                                            <option selected="selected" value="{{$job->type}}">{{$job->type}}</option>
                                            <option value="Contract">Contract</option>
                                            <option value="Full Time">Full Time</option>
                                            <option value="Part Time">Part Time</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="salary">{{__('Salary: ')}}</label>
                                        <select class="form-control" id="SALARY" disabled name="salary">
                                            <option selected="selected" value="{{$job->salary}}">{{$job->salary}}</option>
                                            <option value="$30000.00">$30,000 - $39,999</option>
                                            <option value="$40000.00">$40,000 - $49,999</option>
                                            <option value="$50000.00">$50,000 - $59,999</option>
                                            <option value="$60000.00">$60,000 - $69,999</option>
                                            <option value="$70000.00">$70,000 - $79,999</option>
                                            <option value="$80000.00">$80,000 - $89,999</option>
                                            <option value="$90000.00">$90,000 - $99,999</option>
                                            <option value="$100000.00">$100,000 - $109,999</option>
                                            <option value="$110000.00">$110,000 - $119,999</option>
                                            <option value="$120000.00">$120,000 - $129,999</option>
                                            <option value="$130000.00">$130,000 - $139,999</option>
                                            <option value="$140000.00">$140,000 - $149,999</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="industry">{{__('Industry: ')}}</label>
                                        <select class="form-control" id="INDUSTRY" disabled name="industry">
                                        	<option selected="selected" value="{{$job->industry}}">{{$job->industry}}</option>
                                            <option value=""></option>
                                            <option value="Aerospace">Aerospace</option>
                                            <option value="Agriculture">Agriculture</option>
                                            <option value="Art">Art</option>
                                            <option value="Construction">Construction</option>
                                            <option value="Education">Education</option>
                                            <option value="Energy">Energy</option>
                                            <option value="Entertainment">Entertainment</option>
                                            <option value="Food">Food</option>
                                            <option value="Hospitality">Hospitality</option>
                                            <option value="Manufacturing">Manufacturing</option>
                                            <option value="Medicine">Medicine</option>
                                            <option value="Mining">Mining</option>
                                            <option value="Music">Music</option>
                                            <option value="News">News</option>
                                            <option value="Pharmaceutical">Pharmaceutical</option>
                                            <option value="Technology">Technology</option>
                                            <option value="Telecommunication">Telecommunication</option>
                                            <option value="Transport">Transport</option>
                                        </select>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                        <label for="city">{{__('City: ')}}</label>
                                        <input class="form-control" id="CITY" type="text" name="city" disabled value="{{$job->city}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="state">{{__('State: ')}}</label>
                                        <select class="form-control" id="STATE" disabled name="state">
                                            <option value="{{$job->state}}" selected="selected">{{$job->state}}</option>
                                            <option value="AL">Alabama</option>
                                            <option value="AK">Alaska</option>
                                            <option value="AZ">Arizona</option>
                                            <option value="AR">Arkansas</option>
                                            <option value="CA">California</option>
                                            <option value="CO">Colorado</option>
                                            <option value="CT">Connecticut</option>
                                            <option value="DE">Delaware</option>
                                            <option value="DC">District Of Columbia</option>
                                            <option value="FL">Florida</option>
                                            <option value="GA">Georgia</option>
                                            <option value="HI">Hawaii</option>
                                            <option value="ID">Idaho</option>
                                            <option value="IL">Illinois</option>
                                            <option value="IN">Indiana</option>
                                            <option value="IA">Iowa</option>
                                            <option value="KS">Kansas</option>
                                            <option value="KY">Kentucky</option>
                                            <option value="LA">Louisiana</option>
                                            <option value="ME">Maine</option>
                                            <option value="MD">Maryland</option>
                                            <option value="MA">Massachusetts</option>
                                            <option value="MI">Michigan</option>
                                            <option value="MN">Minnesota</option>
                                            <option value="MS">Mississippi</option>
                                            <option value="MO">Missouri</option>
                                            <option value="MT">Montana</option>
                                            <option value="NE">Nebraska</option>
                                            <option value="NV">Nevada</option>
                                            <option value="NH">New Hampshire</option>
                                            <option value="NJ">New Jersey</option>
                                            <option value="NM">New Mexico</option>
                                            <option value="NY">New York</option>
                                            <option value="NC">North Carolina</option>
                                            <option value="ND">North Dakota</option>
                                            <option value="OH">Ohio</option>
                                            <option value="OK">Oklahoma</option>
                                            <option value="OR">Oregon</option>
                                            <option value="PA">Pennsylvania</option>
                                            <option value="RI">Rhode Island</option>
                                            <option value="SC">South Carolina</option>
                                            <option value="SD">South Dakota</option>
                                            <option value="TN">Tennessee</option>
                                            <option value="TX">Texas</option>
                                            <option value="UT">Utah</option>
                                            <option value="VT">Vermont</option>
                                            <option value="VA">Virginia</option>
                                            <option value="WA">Washington</option>
                                            <option value="WV">West Virginia</option>
                                            <option value="WI">Wisconsin</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="job_id" value="{{$job->id}}">
                                    <div class="row">
                                        <div class="col">

                                            <button class="btn btn-primary btn-block" type="submit" >Apply Now</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
