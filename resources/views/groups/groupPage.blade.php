<?php
/* CST-256 Database Application Programming III
 * Milestone 3
 * profile, Version 1
 * Group CLC Project
 * 10/05/2019
 * This will display the group page
 */
?>

@extends('layouts.app')

@section('title') Group Page @endsection

@section('content')

<div class="jumbotron jumbotron-fluid">
  <div class="container">

    <h1 class="display-4">{{$group_id->name}}</h1>
    <h3 class="lead">{{$group_id->description}}</h3> 
  </div>
</div>


<div class="container">
	<a class="btn btn-primary btn-lg" href="{{ Route('newPost', ['id'=>$group_id->id]) }}" role="button">Create Post</a>
	    <div class="row justify-content-left">
			<div class="col-8">
			<div class="card" style="width: 32rem;">
    			<h1 class="card-header">News Feed</h1>
                <div class="card-body">
                    <div class="row">
                        
                        <div class="col">
                        @if ($errors->any())
    					<div class="alert alert-danger">
    						<ul>
        					@foreach ($errors->all() as $error)
          					<li>{{ $error }}</li>
        					@endforeach
    						</ul>
    						</div><br />
    						@endif
    						<div id="grouppost" class="row">
                    		<div class="col-lg">
							@if (count($group_posts) == 0)
								<h4>No Posts Found!!!</h4>
								
							@else
    							@foreach($group_posts as $gu)
                				
                        			<h4 ><a id="user">{{$gu->body}}</a></h4>
                			
                			</div>
                			</div>
                			<hr>
            			@endforeach
            			@endif
    						
    						
                        </div>
                    </div>
                </div>
                </div>	
                </div>	
                </div>
                </div>
			
			<br>
			<div class="col-8">
				<div class="card"  style="width: 18rem;">
			
				<h1 class="card-header">Members</h1>
	            <div class="card-body">
                    <div class="row">
					
                        <div class="col">
                        @if ($errors->any())
    					<div class="alert alert-danger">
    						<ul>
        					@foreach ($errors->all() as $error)
          					<li>{{ $error }}</li>
        					@endforeach
    						</ul>
    						</div><br />
    					@endif
    						
    						<div id="group" class="row">
                    		<div class="col-lg">
							@if (count($group_users) == 0)
								<h4>No Members Found!!!</h4>
								
							@else
    							@foreach($group_users as $gu)
                				
                        			<h4 ><a id="user">{{$gu->name}}</a></h4>
                			
                			</div>
                			</div>
                			<hr>
            			@endforeach
            			@endif
    						
    						
                     </div>  
                    </div>
                </div>			
			</div>
			</div>	
			</div>		
		</div>





@endsection