<?php

/* CST-256 Database Application Programming III
 * Milestone 3
 * Admin index, Version 1
 * Group CLC Project
 * 10/05/2019
 * This displays a form for the user to create a new post to the group page
 */
?>

@extends('layouts.app')

@section('title') New Post @endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <h4 class="card-header">New Post</h4>
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                            @if ($errors->any())
      						<div class="alert alert-danger">
        						<ul>
            					@foreach ($errors->all() as $error)
              					<li>{{ $error }}</li>
            					@endforeach
        						</ul>
      							</div><br />
    							@endif
    							
    							<form method="post" action="{{route('createPost')}}">
                    				@csrf   
                                  	<div class="form-group">
                                  		 <label for="postSubject">Subject</label>
                                	 	 <input class="form-control" id="SUBJECT" type="text" name="subject">
                                	 	 <br>
                                		 <label for="postBody">Post</label>
                                   	   <textarea class="form-control" id="BODY" name="body" rows="3"></textarea>
                                   	   <input type="hidden" name="groupid" value="{{$id}}">
                                  	</div>
                                  	<button type="submit" class="btn btn-primary">Submit</button>
                                </form>
    							
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>



<br>

<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
@endsection