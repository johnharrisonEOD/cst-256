<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * Company, Version 1
 * Group CLC Project
 * 10/27/2019
 * This model used to build the company profile/ object
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //

    protected $fillable = [
        'name', 'email', 'type', 'description', 'city', 'state', 'user_id'
    ];
}
