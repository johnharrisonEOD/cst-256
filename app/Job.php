<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * Job, Version 1
 * Group CLC Project
 * 10/27/2019
 * This model is used to build job objects
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model implements \JsonSerializable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'salary', 'industry', 'responsibilities', 'requirements', 'city', 'state', 'type', 'company_id'
    ];

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
