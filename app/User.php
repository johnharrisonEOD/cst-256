<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * User, Version 1
 * Group CLC Project
 * 10/27/2019
 * This model used to build the user object
 *  */

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements \JsonSerializable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    ///////////////////////////////////////////////////////////////////////////////////////
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'gender', 'city', 'state', 'usert', 'status', 'firstName', 'lastName',
        'startDate', 'endDate', 'companyName', 'position', 'jobDuties', 'completionDate', 'universityName', 'major'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
