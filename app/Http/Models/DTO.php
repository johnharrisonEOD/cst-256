<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * DTO, Version 1
 * Group CLC Project
 * 10/27/2019
 * This model used to create an object for data transfer
 *  */


namespace App\Http\Models;


class DTO implements \JsonSerializable
{

    public $errorCode, $errorMessage, $data;

    public function __construct($errorCode, $errorMessage, $data)
    {
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
        $this->data = $data;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
