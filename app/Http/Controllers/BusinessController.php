<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * BusinessController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used create, open, edit, update and destroy jobs
 */

namespace App\Http\Controllers;

use App\Company;
use App\Job;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;

class BusinessController extends Controller
{
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
    }
    
    public function createJobView()
    {
        try{
            $this->logger->info("BusinessController createJobView, Accessed by user ". Auth::user()->name . ".");
            $data = (['company' => DB::table('companies')->where('user_id', Auth::user()->id)->first()]);
            return view('post_job')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController createJobView error: " . $e->getMessage());
            return view('error');
        }
    }

    public function createJob(Request $request)
    {
        $request->validate
        ([
            'title'=>'required',
            'type'=>'required',
            'salary'=>'required',
            'industry'=>'required',
            'city'=>'required',
            'state'=>'required',
            'responsibilities'=>'required',
            'requirements'=>'required',
            'company_id'=>'required'
        ]);
        
        try{
            $this->logger->info("BusinessController createJob, Accessed by user ". Auth::user()->name . ".");
            $job = new Job
            ([
                'name'=>$request->post('title'),
                'salary'=>$request->post('salary'),
                'industry'=>$request->post('industry'),
                'responsibilities'=>$request->post('responsibilities'),
                'requirements'=>$request->post('requirements'),
                'city'=>$request->post('city'),
                'state'=>$request->post('state'),
                'type'=>$request->post('type'),
                'company_id'=>$request->post('company_id'),
            ]);
            $job->save();
            return redirect('/')->with('success','Job posted successfully');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController createJob error: " . $e->getMessage());
            return view('error');
        }
    }

    public function openJobsView()
    {
        try{
            $this->logger->info("BusinessController openJobsView, Accessed by user ". Auth::user()->name . ".");
            $user = Auth::user();
            if($user->usert != 'Administrator')
            {
                $company = DB::table('companies')->where('user_id', $user->id)->first();
                $jobs = DB::table('jobs')->get()->where('company_id',$company->id);
                $data = (['jobs' => $jobs, 'user' => $user, 'company' => $company]);
            }
            else
            {
                $data = (['jobs' => Job::all(), 'user' => $user, 'company' => Company::all()]);
            }
            
            return view('openJobs')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController openJobsView error: " . $e->getMessage());
            return view('error');
        }
    }


    public function editJobView(Request $request)
    {
        try{
            $this->logger->info("BusinessController editJobView, Accessed by user ". Auth::user()->name . ".");
            $jobID = $request->get('jobID');
            $job = Job::find($jobID);
            $data = (['job' => $job]);
            
            return view('editJob')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController editJobView error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate
        ([
            'title'=>'required',
            'type'=>'required',
            'salary'=>'required',
            'city'=>'required',
            'state'=>'required',
            'responsibilities'=>'required',
            'requirements'=>'required',
            'company_id'=>'required',
            'industry'=>'required'
        ]);
        
        try{
            $this->logger->info("BusinessController update, Accessed by user ". Auth::user()->name . ".");
            $job = Job::find($id);
            $job->name = $request->post('title');
            $job->salary = $request->post('salary');
            $job->responsibilities = $request->post('responsibilities');
            $job->requirements = $request->post('requirements');
            $job->industry = $request->post('industry');
            $job->city = $request->post('city');
            $job->state = $request->post('state');
            $job->type = $request->post('type');
            $job->company_id = $request->post('company_id');
            
            $job->save();
            return redirect('/openJobs')->with('success', 'Job Updated!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController update error: " . $e->getMessage());
            return view('error');
        }
    }

    // Deletes job posting
    public function destroy($id)
    {
        try{
            $this->logger->info("BusinessController destroy, Accessed by user ". Auth::user()->name . ".");
            $job = Job::find($id);
            $job->delete();
            
            return redirect('/openJobs')->with('success', 'Job Closed');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController destroy error: " . $e->getMessage());
            return view('error');
        }
    }

    public function registerCompanyView()
    {
        try{
            $this->logger->info("BusinessController registerCompanyView, Accessed by user ". Auth::user()->name . ".");
            $data = (['user' => Auth::user()]);
            return view('auth.registerCompany')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController registerCompanyView error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Create a new Company instance after validation
     *
     * @param array $data
     * @return Company instance
     */
    protected function registerCompany(Request $data)
    {
        $data->validate
        ([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:companies'],
            'city' => ['required', 'string', 'max:255'],
            'state' => ['required', 'string', 'max:255'],
            'user_id' => ['required']
        ]);

        try{
            $this->logger->info("BusinessController registerCompany, Accessed by user ". Auth::user()->name . ".");
            $company = new Company([
                'name' => $data['name'],
                'email' => $data['email'],
                'type' => $data['type'],
                'description' => $data['description'],
                'city' => $data['city'],
                'state' => $data['state'],
                'user_id' => $data['user_id']
            ]);
            
            $company->save();
            return redirect('/')->with('success','Company registered successfully');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController registerCompany error: " . $e->getMessage());
            return view('error');
        }
    }
    
    //Allows user to apply for a job and adds it to the applications table
    public function userApply(Request $request)
    {
        try{
            $this->logger->info("BusinessController userApply, Accessed by user ". Auth::user()->name . ".");
            $service = new DatabaseService();
            $user = Auth::user();
            $user_id = $user->id;
            $job_id = $request->get('job_id');
            $service->jobApply($user_id, $job_id);
            return redirect('/')->with('success', 'You have successfully applied!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception BusinessController userApply error: " . $e->getMessage());
            return view('error');
        }
    }
}
