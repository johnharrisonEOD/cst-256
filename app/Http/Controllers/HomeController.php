<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * HomeController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used to configure the home page.
 */

namespace App\Http\Controllers;

use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;
use Illuminate\Support\Facades\Auth;
use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        try{
            $this->logger->info("HomeController index, Accessed by user ". Auth::user()->name . ".");
            $service = new DatabaseService();
            $user_id = Auth::user()->id;
            $interests = $service->randomInterests($user_id);
            $colors = ['warning','primary','success','dark','info','danger'];
            $data = (['interests'=>$interests,'colors'=>$colors]);
            return view('home')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception HomeController index error: " . $e->getMessage());
            return view('error');
        }
    }
}
