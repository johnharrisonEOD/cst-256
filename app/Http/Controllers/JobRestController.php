<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * jobRestController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used for the Job REST API.
 */

namespace App\Http\Controllers;

use App\Http\Models\DTO;
use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;
use Exception;


class JobRestController extends Controller
{
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
        $this->middleware('auth');
    }
    
    public function index()
    {
        try{
            $this->logger->info("JobRestController index request (All Jobs).");
            $service = new DatabaseService();
            $data = $service->getAllJobs();
            $dto = new DTO(200, 'Data gathered', $data);
            
            return json_encode($dto);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception JobRestController index (All Jobs) error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $this->logger->info("JobRestController show request {id}. Requested ID: ". $id);
            $service = new DatabaseService();
            $data = $service->getJobByID($id);
            $dto = new DTO(200, 'Data gathered', $data);
            
            return json_encode($dto);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception JobRestController show {id} error: " . $e->getMessage());
            return view('error');
        }
    }
}
