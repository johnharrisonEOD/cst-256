<?php
/* CST-256 Database Application Programming III
 * Milestone 2
 * LoginController, Version 1
 * Group CLC Project
 * 09/22/2019
 * This controller is used to authenticate the user
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Service\Utility\ILoggerService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $logger;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ILoggerService $logger)
    {
        $this->logger = $logger;
        try{
            $this->logger->info("This is a message from the loginController.");
            $this->middleware('guest')->except('logout');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception LoginController error " . $e->getMessage());
        }
    }
}
