<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * PostController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used create posts for affinity group page.
 */

namespace App\Http\Controllers;


use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class PostController extends Controller
{
    /**
     * Where to redirect users after post is created.
     *
     * @var string
     */
    protected $redirectTo = '/';      
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
    }
    
    
    //used to create the post
    public function createNewPost(Request $request)
    {
        $request->validate([
            'subject'=>['required', 'max:75'],
            'body'=>['required', 'max:255']]);
        
        try{
            $this->logger->info("PostController createNewPost, Accessed by user ". Auth::user()->name . ".");
            $service = new DatabaseService();
            $user_id = Auth::user()->id;
            $group_id = $request->get('groupid');
            $post_sub = $request->get('subject');
            $post_bod = $request->get('body');
            
            //update database
            $service->savePost($user_id, $group_id, $post_sub, $post_bod);
            return redirect($group_id.'/groupPage')->with('success', 'Post Created!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception PostController createNewPost error: " . $e->getMessage());
            return view('error');
        }
        
    }

    
    
    public function newPost($id){
        try{
            $this->logger->info("PostController newPost, Accessed by user ". Auth::user()->name . ".");
            return view('groups.newPost')->with('id',$id);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception PostController newPost error: " . $e->getMessage());
            return view('error');
        }
    }
    
}
