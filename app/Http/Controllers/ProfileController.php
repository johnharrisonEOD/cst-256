<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * ProfileController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used to get and update the user profile
 */

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\User;
use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;


class ProfileController extends Controller
{
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
    }
    
    
    //
    public function index()
    {
        try{
            $this->logger->info("ProfileController index, Accessed by user ". Auth::user()->name . ".");
            $user = Auth::user();
            return view('profile', compact('user'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController index error: " . $e->getMessage());
            return view('error');
        }
    }

    public function findJobView(Request $request)
    {
        try{
            $this->logger->info("ProfileController findJobView, Accessed by user ". Auth::user()->name . ".");
            $user = Auth::user();
            $tableReq = $request->get('jobtablefilter');
            $tableSearch = $request->get('jobsearch');
            $service = new DatabaseService();
            
            $data = $service->jobSearch($tableReq, $tableSearch, $user);
            return view('find_job')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController findJobView error: " . $e->getMessage());
            return view('error');
        }
    }

    
    
    public function viewJob($id)
    {
        try{
            $this->logger->info("ProfileController viewJob, Accessed by user ". Auth::user()->name . ".");
            $job = Job::find($id);
            return view('view_job', compact('job'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController viewJob error: " . $e->getMessage());
            return view('error');
        }
    }

    public function portfolio()
    {
        try{
            $this->logger->info("ProfileController portfolio, Accessed by user ". Auth::user()->name . ".");
            $user = Auth::user();
            return view('portfolio', compact ('user'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController portfolio error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //used to create a user portfolio
    public function createPortfolio(Request $request)
    {
        $request->validate([
            'firstName'=>'required',
            'lastName'=>'required',
        ]);

        try{
            $this->logger->info("ProfileController createPortfolio, Accessed by user ". Auth::user()->name . ".");
            $user = User::find(auth::user()->id);
            $user->firstName = $request->get('firstName');
            $user->lastName = $request->get('lastName');
            $user->startDate = $request->get('startDate');
            $user->endDate = $request->get('endDate');
            $user->companyName = $request->get('companyName');
            $user->position = $request->get('position');
            $user->jobDuties = $request->get('jobDuties');
            $user->completionDate = $request->get('completionDate');
            $user->universityName = $request->get('universityName');
            $user->major = $request->get('major');
            
            $user->save();
            return redirect('/home')->with('success', 'Portfolio Successfully Created!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController createPortfolio error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);

        try{
            $this->logger->info("ProfileController update, Accessed by user ". Auth::user()->name . ".");
            $user = User::find(auth::user()->id);
            $user->name =  $request->get('name');
            $user->email = $request->get('email');
            $user->password = $request->get('password');
            $user->phone =  $request->get('phone');
            $user->gender = $request->get('gender');
            $user->city =  $request->get('city');
            $user->state = $request->get('state');
            $user->save();
            
            return redirect('profile')->with('success', 'Profile Updated!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController update error: " . $e->getMessage());
            return view('error');
        }
    }

    public function addInterests(Request $request)
    {
        try{
            $this->logger->info("ProfileController addInterests, Accessed by user ". Auth::user()->name . ".");
            if($request->ajax())
            {
                //create service
                $service = new DatabaseService();
                
                //get user
                $user = Auth::user();
                $id = $user->id;
                
                //get user interests
                $interest = $request->get('interests');
                
                //insert interest into database
                $service->saveUserInterests($id, $interest);
            }
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception ProfileController addInterests error: " . $e->getMessage());
            return view('error');
        }

    }

}
