<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * AdminController, Version 4
 * Group CLC Project
 * 10/27/2019
 * This controller is used read, edit and delete user profiles and affinity groups. It can also edit a user portfolio
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Group;
use App\User;
use App\Service\Utility\ILoggerService;

class AdminController extends Controller
{ 
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
    }
    
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $this->logger->info("Admin Controller Index, Accessed by user ". Auth::user()->name . ".");
            $users = User::all();
            $groups = Group::all();
            
            return view('Admin.index', compact('users', 'groups'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController error: " . $e->getMessage());
            return view('error');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $this->logger->info("Admin Controller Create, Accessed by user ". Auth::user()->name . ".");
            return view('Admin.createUser');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController Create error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //used to save the user data from the form
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);
        try{
            $this->logger->info("Admin Controller store, Accessed by user ". Auth::user()->name . ".");
            $user = new User([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $request->get('password'),
                'phone' => $request->get('phone'),
                'gender' => $request->get('gender'),
                'city' => $request->get('city'),
                'state' => $request->get('state'),
                'usert' => $request->get('usert'),
                'status' => $request->get('status')
            ]);
            $user->save();
            return redirect('/admin')->with('success', 'User Successfully Created!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController store error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    //method used to get and edit a single user profile
    public function edit($id)
    {   
        try{
            $this->logger->info("Admin Controller edit, Accessed by user ". Auth::user()->name . ".");
            $user = User::find($id);
            return view('Admin.editUser', compact('user'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController edit error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    //gets the form data post save and updates the database
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]);
        try{
            $this->logger->info("Admin Controller update, Accessed by user ". Auth::user()->name . ".");
            $user = User::find($id);
            $user->name =  $request->get('name');
            $user->email = $request->get('email');
            $user->password = $request->get('password');
            $user->phone =  $request->get('phone');
            $user->gender = $request->get('gender');
            $user->city =  $request->get('city');
            $user->state = $request->get('state');
            $user->usert =  $request->get('usert');
            $user->status = $request->get('status');
            $user->save();
            return redirect('/admin')->with('success', 'User Updated!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController update error: " . $e->getMessage());
            return view('error');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    //Removes the selected user from the data base.
    public function destroy($id)
    {
        try{
            $this->logger->info("Admin Controller destroy Accessed by user ". Auth::user()->name . ".");
            $user = User::find($id);
            $user->delete();
            return redirect('/admin')->with('success', 'User Deleted!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController Destroy error: " . $e->getMessage());
            return view('error');
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    //method used to get and edit a single user portfolio
    public function editUserPortfolio($id)
    {
        try{
            $this->logger->info("Admin Controller editUserPortfolio, Accessed by user ". Auth::user()->name . ".");
            $user = User::find($id);
            return view('Admin.editPortfolio', compact('user'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController editUserPortfolio error: " . $e->getMessage());
            return view('error');
        }
    }
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    //gets the form data post save and updates the database
    public function updatePortfolio(Request $request, $id)
    {
        $request->validate([
            'firstName'=>'required',
            'lastName'=>'required',
        ]);
        try{
            $this->logger->info("Admin Controller updatePortfolio Accessed by user ". Auth::user()->name . ".");
            $user = User::find($id);
            $user->firstName = $request->get('firstName');
            $user->lastName = $request->get('lastName');
            $user->startDate = $request->get('startDate');
            $user->endDate = $request->get('endDate');
            $user->companyName = $request->get('companyName');
            $user->position = $request->get('position');
            $user->jobDuties = $request->get('jobDuties');
            $user->completionDate = $request->get('completionDate');
            $user->universityName = $request->get('universityName');
            $user->major = $request->get('major');
            $user->save();
            return redirect('/admin')->with('success', 'User Portfolio Updated!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController updatePortfolio error: " . $e->getMessage());
            return view('error');
        }
    }
    
    
////////////////Admin Group////////////////////

    //Removes the selected group from the data base.
    public function destroyGroup($id)
    {
        try{
            $this->logger->info("Admin Controller destroyGroup, Accessed by user ". Auth::user()->name . ".");
            $group = Group::find($id);
            $group->delete();
            
            return redirect('/admin')->with('success', 'Group Deleted!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController destroyGroup error: " . $e->getMessage());
            return view('error');
        }
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    //method used to get and edit a single user portfolio
    public function editGroup($id)
    {
        try{
            $this->logger->info("Admin Controller editGroup, Accessed by user ". Auth::user()->name . ".");
            $group = Group::find($id);
            return view('Admin.editGroup', compact('group'));
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController editGroup error: " . $e->getMessage());
            return view('error');
        }
    }
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    //gets the form data post save and updates the database
    public function updateGroup(Request $request, $id)
    {
            $request->validate([
                'name'=>'required',
                'description'=>'required',
            ]);
        
        try{
            $this->logger->info("Admin Controller updateGroup, Accessed by user ". Auth::user()->name . ".");
            $group = Group::find($id);
            $group->name = $request->get('name');
            $group->description = $request->get('description');
            $group->save();
            return redirect('/admin')->with('success', 'Group Updated!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception AdminController updateGroup error: " . $e->getMessage());
            return view('error');
        }
    }
    

}
