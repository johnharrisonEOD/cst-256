<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * UserRestController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used for the User REST API.
 */

namespace App\Http\Controllers;

use App\Http\Models\DTO;
use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;
use Exception;

class UserRestController extends Controller
{
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        try{
            $this->logger->info("UserRestController index request (All Users).");
            $service = new DatabaseService();
            $data = $service->getAllUsers();
            $dto = new DTO(200, 'Data gathered', $data);
            
            return json_encode($dto);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception UserRestController index (All Users) error: " . $e->getMessage());
            return view('error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $this->logger->info("UserRestController show request {id}. Requested ID: ". $id);
            $service = new DatabaseService();
            $data = $service->getUserByID($id);
            $dto = new DTO(200, 'Data gathered', $data);
            
            return json_encode($dto);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception UserRestController show {id} error: " . $e->getMessage());
            return view('error');
        }
    }

}
