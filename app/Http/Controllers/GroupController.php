<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * GroupController, Version 2
 * Group CLC Project
 * 10/27/2019
 * This controller is used create, join or leave an affinity group.
 */

namespace App\Http\Controllers;

use App\Group;
use App\Service\DatabaseService;
use App\Service\Utility\ILoggerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;

class GroupController extends Controller
{
    protected $logger;
    public function __construct(ILoggerService $logger){
        $this->logger = $logger;
    }
    
    public function index()
    {
        try{
            $this->logger->info("GroupController index, Accessed by user ". Auth::user()->name . ".");
            $groups = $this->recommendedGroups();
            if ($groups->count() == 0)
            {
                $groups = Group::all();
            }
            $users_groups = $this->inGroup();
            $names = array();
            foreach ($users_groups as $g)
            {
                array_push($names,$g->name);
            }
            
            $data = (['groups'=>$groups, 'names'=>$names]);
            return view('findGroups')->with($data);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController index error: " . $e->getMessage());
            return view('error');
        }
    }

    private function recommendedGroups()
    {
        try{
            $this->logger->info("GroupController recommendedGroups, Accessed by user ". Auth::user()->name . ".");
            $user_id = Auth::user()->id;
            $service = new DatabaseService();
            return $service->getRecommendedGroups($user_id);
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController recommendedGroups error: " . $e->getMessage());
            return view('error');
        }
    }

    public function joinGroup(Request $request)
    {
        try{
            $this->logger->info("GroupController joinGroup, Accessed by user ". Auth::user()->name . ".");
            if($request->ajax())
            {
                //create service
                $service = new DatabaseService();
                
                //get user
                $user = Auth::user();
                $user_id = $user->id;
                
                //get group name
                $group_name = $request->get('group');
                
                //update database
                $service->joinGroup($user_id, $group_name);
                //return 'Joined '.$group_name.' successfully.';
                return redirect('/findGroups')->with('Joined '.$group_name.' successfully.');
            }
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController joinGroup error: " . $e->getMessage());
            return view('error');
        }
    }

    public function leaveGroup(Request $request)
    {
        try{
            $this->logger->info("GroupController leaveGroup, Accessed by user ". Auth::user()->name . ".");
            if($request->ajax())
            {
                //create service
                $service = new DatabaseService();
                
                //get user
                $user = Auth::user();
                $user_id = $user->id;
                
                //get group name
                $group_name = $request->get('group');
                
                //update database
                $service->leaveGroup($user_id, $group_name);
                //return 'Left '.$group_name;
                return redirect('/findGroups')->with('Left '.$group_name);
            }
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController leaveGroup error: " . $e->getMessage());
            return view('error');
        }
    }

    private function inGroup()
    {
        try{
            $this->logger->info("GroupController inGroup, Accessed by user ". Auth::user()->name . ".");
            $service = new DatabaseService();
            
            $user_id = Auth::user()->id;
            
            $results = $service->usersGroups($user_id);
            
            return $results;
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController inGroup error: " . $e->getMessage());
            return view('error');
        }
    }
    
    
    public function createGroup()
    {
        try{
            $this->logger->info("GroupController createGroup, Accessed by user ". Auth::user()->name . ".");
            return view('createGroup');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController createGroup error: " . $e->getMessage());
            return view('error');
        }
    }
    
    
    public function createNewGroup(Request $request){
        $request->validate([
            'name'=>'required',
            'description'=>'required',
            'interest'=>'required'
        ]);

        try{
            $this->logger->info("GroupController createNewGroup, Accessed by user ". Auth::user()->name . ".");
            //create service
            $service = new DatabaseService();
            
            $group_name = $request->get('name');
            $group_desc = $request->get('description');
            $group_inter = $request->get('interest');
            
            //update database
            $service->createGroup($group_name, $group_desc, $group_inter);
            
            //return view('createGroup');
            return redirect('/createGroups')->with('success', 'Group Successfully Created!');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController createNewGroup error: " . $e->getMessage());
            return view('error');
        }
        
    }
    

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function goToGroup($id)
    {
        try{
            $this->logger->info("GroupController goToGroup, Accessed by user ". Auth::user()->name . ".");
            $service = new DatabaseService();
            $group_id = Group::find($id);
            //$user_id = Auth::user();
            $group_users = $service->groupUsers($group_id);
            $group_posts = $service->groupPosts($group_id);
            
            
            //return view('groups.groupPage', compact('group_id'));
            return view('groups.groupPage')->with('group_id',$group_id)
            ->with('group_users',$group_users)
            ->with('group_posts',$group_posts);
            //return view('groups.groupPage');
        }
        //catch exception
        catch(Exception $e) {
            $this->logger->error("Exception GroupController goToGroup error: " . $e->getMessage());
            return view('error');
        }
    }
}
