<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * Posts, Version 1
 * Group CLC Project
 * 10/27/2019
 * This model used to build the post/ object
 *  */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject', 'body', 'groupID', 'userID'
    ];
}
