<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * AppServiceProvider, Version 2
 * Group CLC Project
 * 10/27/2019
 * Added ILoggerService
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Service\Utility\MyLogger3;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Service\Utility\ILoggerService', function ($app) {
            return new MyLogger3();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
