<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * DatabaseService, Version 1
 * Group CLC Project
 * 10/27/2019
 * This service is used to access the database
 */


namespace App\Service;

use App\Company;
use App\Job;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DatabaseService
{
    public function saveUserInterests($user_id, $interest)
    {
        DB::table('users_interests')->insert
        ([
            'user_id' => $user_id,
            'interest' => $interest
        ]);
    }

    public function saveGroupInterests($group_id, $interest)
    {
        DB::table('groups_interests')->insert
        ([
            'group_id' => $group_id,
            'interest' => $interest
        ]);
    }

    public function getRecommendedGroups($user_id)
    {
        $results = DB::table('groups')
        ->select('users.name', 'groups.name',DB::raw('COUNT(*) as inCommon'),'groups.description', 'groups.id')
        ->join('groups_interests','groups.id', '=', 'groups_interests.group_id')
        ->join('users_interests', 'groups_interests.interest', '=', 'users_interests.interest')
        ->join('users','users_interests.user_id', '=', 'users.id')
        ->where('users.id', '=', $user_id)
        ->groupBy(['users.name','groups.name','groups.description','groups.id'])
        ->orderBy('inCommon', 'desc')
        ->get();

        return $results;
    }

    public function randomInterests($user_id)
    {
        $results = DB::select(DB::raw("SELECT interests.* FROM interests
        LEFT JOIN users_interests ON interests.interest = users_interests.interest
        LEFT JOIN users ON users_interests.user_id = users.id
        WHERE users_interests.interest NOT IN (SELECT users_interests.interest FROM users_interests WHERE users_interests.user_id = '$user_id') OR users_interests.interest IS NULL
        LIMIT 20;"));

        return $results;
    }

    public function joinGroup($user_id, $group_name)
    {
        $group_id = DB::table('groups')
        ->select('groups.id')
        ->where('groups.name','=', $group_name )
        ->first();

        DB::table('users_groups')->insert
        ([
            'group_id' => $group_id->id,
            'user_id' => $user_id
        ]);
    }

    public function leaveGroup($user_id, $group_name)
    {
        $group_id = DB::table('groups')
        ->select('groups.id')
        ->where('groups.name','=', $group_name )
        ->first();

        DB::table('users_groups')
        ->where('group_id','=',$group_id->id)
        ->where('users_groups.user_id','=', $user_id)
        ->delete();
    }

    public function usersGroups($user_id)
    {
        $groups = DB::table('groups')
        ->select('groups.*')
        ->join('users_groups','groups.id', '=', 'users_groups.group_id')
        ->join('users','users_groups.user_id','=','users.id')
        ->where('users.id','=',$user_id)
        ->get();

        return $groups;
    }

    public function createGroup($group_name, $group_desc, $group_inter)
    {
        DB::insert('insert into groups (name, description) values (?, ?)', array($group_name, $group_desc));
        //DB::insert('insert into interests (interest) values (?)', [$group_inter]);

        $group_id = DB::table('groups')
        ->select('groups.id')
        ->where('groups.name','=', $group_name )
        ->first();

        DB::table('groups_interests')->insert
        ([
            'group_id' => $group_id->id,
            'interest' => $group_inter
        ]);
    }

    public function groupUsers($group_id)
    {
        return DB::table('users')->select('users.id','users.name')
        ->join('users_groups','users_groups.user_id','=', 'users.id')
        ->where('users_groups.group_id','=', $group_id->id)
        ->get();
    }

    public function groupPosts($group_id)
    {
        return DB::table('posts')->select('posts.id','posts.body','posts.group_id')
        ->join('groups','groups.id','=', 'posts.group_id')
        ->where('groups.id','=', $group_id->id)
        ->get();
    }



    public function savePost($user_id, $group_id, $post_sub, $post_bod)
    {
        DB::insert('insert into posts (subject, body, group_id, user_id) values (?, ?, ?, ?)', array($post_sub, $post_bod, $group_id, $user_id));

    }


    public function jobSearch($tableReq, $tableSearch, $user)
    {
        if ($tableReq == "Recommended"){
            if ($tableSearch == ""){
                $jobs = DB::table('jobs')->select('jobs.*')
                ->join('users_interests', 'users_interests.interest','=', 'jobs.industry')
                ->where('users_interests.user_id','=', $user->id)
                ->get();

                $data = (['jobs' => $jobs, 'user' => $user, 'companies' => Company::all()]);
                return $data;
            }
            else{
                $jobs = DB::table('jobs')->select('jobs.*')
                ->join('users_interests', 'users_interests.interest','=', 'jobs.industry')
                ->where('users_interests.user_id','=', $user->id)
                ->where('jobs.name','like', "%{$tableSearch}%")
                ->orWhere('jobs.responsibilities','like', "%{$tableSearch}%")
                ->get();

                $data = (['jobs' => $jobs, 'user' => $user, 'companies' => Company::all()]);
                return $data;
            }
        }
        else{
            if ($tableSearch == ""){
                $data = (['jobs' => Job::all(), 'user' => Auth::user(), 'companies' => Company::all()]);
                return $data;
            }
            else{
                $jobs = DB::table('jobs')->select('jobs.*')
                ->where('jobs.name','like', "%{$tableSearch}%")
                ->orWhere('jobs.responsibilities','like', "%{$tableSearch}%")
                ->get();
                $data = (['jobs' => $jobs, 'user' => $user, 'companies' => Company::all()]);
                return $data;
            }
        }

    }

    public function jobApply($user_id, $job_id)
    {
        DB::insert('insert into applications (user_id, job_id) values (?, ?)', array($user_id, $job_id));
    }

    public function getAllUsers()
    {
        $users = User::query()->select(['*'])->limit(15)->get();
        return $users->toArray();
    }

    public function getUserByID($id)
    {
        $user = User::query()->select(['*'])->where('id','=',$id)->get();
        return $user->toArray();
    }

    public function getAllJobs()
    {
        $jobs = Job::query()->select(['*'])->limit(15)->get();
        return $jobs->toArray();
    }

    public function getJobByID($id)
    {
        $job = Job::query()->select(['*'])->where('id','=',$id)->get();
        return $job->toArray();
    }

}
