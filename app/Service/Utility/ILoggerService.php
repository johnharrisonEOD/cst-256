<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * ILoggerService, Version 1
 * Group CLC Project
 * 10/27/2019
 * Interface used for the logger service.
 */

namespace App\Service\Utility;

interface ILoggerService
{
    public function debug($Message);
    public function info($Message);
    public function warning($Message);
    public function error($Message);
    
}

