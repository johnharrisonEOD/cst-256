<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * MyLogger1, Version 1
 * Group CLC Project
 * 10/27/2019
 * Class used for application logging.
 */

namespace App\Service\Utility;

//use App\Services\Utility\ILogger;
use Illuminate\Support\Facades\Log;

class MyLogger1 implements ILogger
{

    public static function getLogger(){
        return;
    }
    
    public function debug($classMessage){
        Log::debug($classMessage);
    }   

    public function warning($classMessage){
        Log::warning($classMessage);
    }

    public function error($classMessage){
        Log::error($classMessage);
    }

    public function info($classMessage){
        Log::info($classMessage);
    } 
    
}

