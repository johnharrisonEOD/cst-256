<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * ILogger, Version 1
 * Group CLC Project
 * 10/27/2019
 * Interface used for the logging.
 */

namespace App\Service\Utility;

interface ILogger
{
    public static function getLogger();
    public function debug($Message);
    public function info($Message);
    public function warning($Message);
    public function error($Message);
    
}

