<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * MyLogger3, Version 1
 * Group CLC Project
 * 10/27/2019
 * Class used for application logging.
 */

namespace App\Service\Utility;

//use App\Services\Utility\ILogger;
use Illuminate\Support\Facades\Log;

class MyLogger3 implements ILoggerService
{
    public function debug($classMessage){
        Log::debug($classMessage);
    }   

    public function warning($classMessage){
        Log::warning($classMessage);
    }

    public function error($classMessage){
        Log::error($classMessage);
    }

    public function info($classMessage){
        Log::info($classMessage);
    } 
    
}

