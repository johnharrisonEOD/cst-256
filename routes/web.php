<?php
/* CST-256 Database Application Programming III
 * Milestone 7
 * web, Version 5
 * Group CLC Project
 * 10/27/2019
 * This is used to register verious routes for our web application
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();
// Home Controller
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'ProfileController@addInterests');

// REST *** Placed here for test purposes ***
Route::resource('/restjob','JobRestController');  // W/o id parameter: returns at most 15 job. W/ id parameter: return a single job with corresponding id. In JSON.
Route::resource('/restuser','UserRestController');  // W/o id parameter: returns at most 15 users. W/ id parameter: return a single user with corresponding id. In JSON.

//Route used to display the generic error page.
Route::get('error', function() {
    return view('error');
});

Route::group(
    array (
        'middleware' => ['auth']
    ),
    function () {

        // Profile Controller
        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::get('/profile/update', 'ProfileController@update')->name('update');
        Route::get('/portfolio', 'ProfileController@portfolio')->name('portfolio');
        Route::get('createPortfolio', 'ProfileController@createPortfolio')->name('createPortfolio');
        Route::get('/findJob','ProfileController@findJobView')->name('findJob');
        Route::get('/{id}/viewJob','ProfileController@viewJob')->name('viewJob');

        // Business Controller
        Route::get('/createJob','BusinessController@createJobView')->name('createJob');
        Route::post('/createJob','BusinessController@createJob')->name('createJob');
        Route::get('/registerCompany','BusinessController@registerCompanyView')->name('registerCompany');
        Route::post('/registerCompany','BusinessController@registerCompany')->name('registerCompany');
        Route::get('/openJobs','BusinessController@openJobsView')->name('openJobs');
        Route::get('/editJob','BusinessController@editJobView')->name('editJob');
        Route::post('/userApply','BusinessController@userApply')->name('userApply');
        Route::resource('company','BusinessController');

        // Admin Controller
        //Route::get('/admin', 'AdminController@index')->name('admin');
        Route::resource('admin', 'AdminController');
        Route::get('admin', 'AdminController@index')->name('admin');
        Route::get('create', 'AdminController@create')->name('create');
        Route::get('admin/{id}/editUserPortfolio', 'AdminController@editUserPortfolio')->name('editUserPortfolio');
        Route::post('admin/updatePortfolio/{id}', 'AdminController@updatePortfolio')->name('updatePortfolio');
        Route::delete('admin/destroyGroup/{id}', 'AdminController@destroyGroup')->name('destroyGroup');
        Route::get('admin/{id}/editGroup', 'AdminController@editGroup')->name('editGroup');
        Route::post('admin/updateGroup/{id}', 'AdminController@updateGroup')->name('updateGroup');

        // Group Controller
        Route::resource('group', 'GroupController');
        Route::get('{id}/groupPage', 'GroupController@goToGroup')->name('groupPage');
        Route::get('findGroups', 'GroupController@index')->name('findGroups');
        Route::post('findGroups', 'GroupController@joinGroup');
        Route::post('leaveGroups', 'GroupController@leaveGroup');
        Route::get('/createGroups', 'GroupController@createGroup')->name('createGroup');
        Route::post('createGroups', 'GroupController@createNewGroup')->name('createNewGroup');

        //Posts Controller
        Route::resource('post', 'PostController');
        Route::post('createPost', 'PostController@createNewPost')->name('createPost');
        //Route::get('newPost/{id}', ['as'=>'newPost', 'uses'=>'PostController@newPost']);
        Route::get('newPost/{id}', 'PostController@newPost')->name('newPost');
    }
    );
